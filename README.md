# Tributos NG

Front-End de cálculo de multa e juros de tributos. Depende do projeto [https://gitlab.com/coletoria_megasoft/tributos-api](https://gitlab.com/coletoria_megasoft/tributos-api)

Para iniciar o projeto, primeiro deve clonar o projeto em:
`git clone https://gitlab.com/coletoria_megasoft/tributos-ng.git`

Mudar para a pasta gerada: `cd tributos-ng`

Rodar o comando `npm install` para instalar as dependências.

Rodar o comando `ng serve` para iniciar a aplicação.
Navegar no endereço `http://localhost:4200/`.

> Projeto depende do Node LTS e [Angular CLI](https://github.com/angularangular-cli) versão 8.3.19 ou maior.