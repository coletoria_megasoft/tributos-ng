import { TestBed } from '@angular/core/testing';

import { CalcularTributosService } from './calcular-tributos.service';

describe('CalcularTributosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CalcularTributosService = TestBed.get(CalcularTributosService);
    expect(service).toBeTruthy();
  });
});
