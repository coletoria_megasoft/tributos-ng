import { Contribuinte } from "../contribuintes/contribuinte";
import { Refis } from "./refis";
import { Tributo } from "./tributo";

export class CalculoInput {

  tributo: Tributo;
  contribuinte: Contribuinte;
  dataPagamento: Date;

  constructor() {
    this.tributo = null;
    this.contribuinte = null;
    this.dataPagamento = null;
  }

}
