import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { take } from 'rxjs/operators';
import { Tributo } from './tributo';

@Injectable({
  providedIn: 'root'
})
export class TributosService {

  baseURL = `${environment.API_URL}`;

  constructor(private http: HttpClient) { }

  listar(): Observable<Tributo[]> {
    const url = `${this.baseURL}/tributos`;
    return this.http.get<Tributo[]>(url).pipe();
  }

  getTributoPorId(id: number): Observable<Tributo> {
    const url = `${this.baseURL}/tributos/${id}`
    return this.http.get<Tributo>(url).pipe(take(1))
  }

}
