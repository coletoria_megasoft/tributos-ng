import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { take } from 'rxjs/operators';
import { CalculoInput } from './calculoInput';
import { Resultado } from './resultado';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CalcularTributosService {

  baseURL = `${environment.API_URL}`;

  constructor(private http: HttpClient) { }

  calcularTributo(calculoInput: CalculoInput): Observable<Resultado> {
    const saveUrl = `${this.baseURL}/calculo`;
    return this.http.post<Resultado>(saveUrl, calculoInput).pipe(take(1))
  }

}
