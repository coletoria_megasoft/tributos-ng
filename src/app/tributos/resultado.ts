export class Resultado {

  valorMulta: number;
  valorJuros: number;
  valorTotal: number;

  constructor() {
    this.valorMulta = null;
    this.valorJuros = null;
    this.valorTotal = null;
  }

}
