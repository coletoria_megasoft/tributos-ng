import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaTributosComponent } from './lista-tributos.component';

describe('ListaTributosComponent', () => {
  let component: ListaTributosComponent;
  let fixture: ComponentFixture<ListaTributosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaTributosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaTributosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
