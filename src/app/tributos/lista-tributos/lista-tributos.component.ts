import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Tributo } from '../tributo';
import { TributosService } from '../tributos.service';

@Component({
  selector: 'app-lista-tributos',
  templateUrl: './lista-tributos.component.html',
  styleUrls: ['./lista-tributos.component.css']
})
export class ListaTributosComponent implements OnInit {

  tributos$: Observable<Tributo[]>;

  constructor(private service: TributosService) { }

  ngOnInit() {
    this.tributos$ = this.service.listar();
  }

}
