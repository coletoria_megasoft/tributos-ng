import { TestBed } from '@angular/core/testing';

import { TributosService } from './tributos.service';

describe('TributosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TributosService = TestBed.get(TributosService);
    expect(service).toBeTruthy();
  });
});
