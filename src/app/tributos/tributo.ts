export class Tributo {

  id: number;
  nome: string;
  tipoTributo: string;
  taxaMulta: number;
  taxaJuros: number;

  constructor() {
    this.id = null;
    this.nome = null;
    this.tipoTributo = null;
    this.taxaMulta = null;
    this.taxaJuros = null;
  }

}
