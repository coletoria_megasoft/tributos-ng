import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NgModule, LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localePTBR from '@angular/common/locales/pt';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ListaDestinacoesComponent } from './destinacoes/lista-destinacoes/lista-destinacoes.component';
import { ListaTributosComponent } from './tributos/lista-tributos/lista-tributos.component';
import { ListaContribuintesComponent } from './contribuintes/lista-contribuintes/lista-contribuintes.component';
import { ListaRefisComponent } from './refis/lista-refis/lista-refis.component';
import { ListaDuamsComponent } from './duam/lista-duams/lista-duams.component';
import { AtivoPipe } from './pipes/ativo.pipe';
import { FormDestinacoesComponent } from './destinacoes/form-destinacoes/form-destinacoes.component';
import { FormContribuintesComponent } from './contribuintes/form-contribuintes/form-contribuintes.component';
import { CpfcnpjPipe } from './pipes/cpfcnpj.pipe';

registerLocaleData(localePTBR);

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ListaDestinacoesComponent,
    ListaTributosComponent,
    ListaContribuintesComponent,
    ListaRefisComponent,
    ListaDuamsComponent,
    AtivoPipe,
    FormDestinacoesComponent,
    FormContribuintesComponent,
    CpfcnpjPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [{
    provide: LOCALE_ID,
    useValue: 'pt-br'
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
