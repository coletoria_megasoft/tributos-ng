import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormContribuintesComponent } from './contribuintes/form-contribuintes/form-contribuintes.component';
import { ListaContribuintesComponent } from './contribuintes/lista-contribuintes/lista-contribuintes.component';
import { FormDestinacoesComponent } from './destinacoes/form-destinacoes/form-destinacoes.component';
import { ListaDestinacoesComponent } from './destinacoes/lista-destinacoes/lista-destinacoes.component';
import { ListaDuamsComponent } from './duam/lista-duams/lista-duams.component';
import { HomeComponent } from './home/home.component';
import { ListaRefisComponent } from './refis/lista-refis/lista-refis.component';
import { ListaTributosComponent } from './tributos/lista-tributos/lista-tributos.component';


const routes: Routes = [
  { path: 'tributos', component: ListaTributosComponent },
  
  { path: 'destinacoes', component: ListaDestinacoesComponent },
  { path: 'destinacoes/editar/:id', component: FormDestinacoesComponent },
  { path: 'destinacoes/novo', component: FormDestinacoesComponent },
  
  { path: 'contribuintes', component: ListaContribuintesComponent },
  { path: 'contribuintes/editar/:id', component: FormContribuintesComponent },
  { path: 'contribuintes/novo', component: FormContribuintesComponent },

  { path: 'refis', component: ListaRefisComponent },
  { path: 'duams', component: ListaDuamsComponent },
  
  { path: '', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
