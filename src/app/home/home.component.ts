import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { CalcularTributosService } from '../tributos/calcular-tributos.service';
import { Tributo } from '../tributos/tributo';
import { Resultado } from '../tributos/resultado';
import { TributosService } from '../tributos/tributos.service';
import { CalculoInput } from '../tributos/calculoInput';
import { Mensagem } from './message';
import { ContribuintesService } from '../contribuintes/contribuintes.service';
import { Contribuinte } from '../contribuintes/contribuinte';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  tributos$: Observable<Tributo[]>;
  contribuintes$: Observable<Contribuinte[]>;
  tributo: number = 0;
  contribuinte: number = 0;
  resultado$: Observable<Resultado>;
  mensagem: Mensagem;
  calculoInput: CalculoInput;

  constructor(private tributoService: TributosService,
    private contribuinteService: ContribuintesService, 
    private calculoService: CalcularTributosService ) { }

  ngOnInit() {
    this.tributos$ = this.tributoService.listar();
    this.contribuintes$ = this.contribuinteService.listar();
    this.calculoInput = new CalculoInput();
  }

  calcular() {
    this.limpaMensagem()
    if(this.validaCampos()) {
      this.tributoService.getTributoPorId(this.tributo).subscribe((tributo) => {
        this.calculoInput.tributo = tributo;
        this.contribuinteService.pesquisarPorId(this.contribuinte).subscribe((contribuinte) => {
          this.calculoInput.contribuinte = contribuinte;
          this.resultado$ = this.calculoService.calcularTributo(this.calculoInput);
        });
      });
    }
  }

  validaCampos(): boolean {

    if (!this.tributo) {
      console.log(this.calculoInput.tributo)
      this.mensagem = new Mensagem("O tributo é de preenchimento obrigatório");
      return false;
    }

    if (!this.contribuinte) {
      this.mensagem = new Mensagem(`O contribuinte é de preenchimento obrigatório`);
      return false;
    }

    if (!this.calculoInput.dataPagamento) {
      this.mensagem = new Mensagem(`A Data de Pagamento é de preenchimento obrigatório`);
      return false;
    }

    return true;
  }

  limpaMensagem() {
    this.mensagem = null;
  }

}
