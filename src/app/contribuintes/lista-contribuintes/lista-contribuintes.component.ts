import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Contribuinte } from '../contribuinte';
import { ContribuintesService } from '../contribuintes.service';

@Component({
  selector: 'app-lista-contribuintes',
  templateUrl: './lista-contribuintes.component.html',
  styleUrls: ['./lista-contribuintes.component.css']
})
export class ListaContribuintesComponent implements OnInit {

  contribuintes$: Observable<Contribuinte[]>;

  constructor(private service: ContribuintesService,) { }

  ngOnInit() {
    this.contribuintes$ = this.service.listar();
  }

}
