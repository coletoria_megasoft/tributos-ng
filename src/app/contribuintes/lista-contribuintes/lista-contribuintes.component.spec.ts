import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaContribuintesComponent } from './lista-contribuintes.component';

describe('ListaContribuintesComponent', () => {
  let component: ListaContribuintesComponent;
  let fixture: ComponentFixture<ListaContribuintesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaContribuintesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaContribuintesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
