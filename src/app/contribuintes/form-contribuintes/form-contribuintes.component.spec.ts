import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormContribuintesComponent } from './form-contribuintes.component';

describe('FormContribuintesComponent', () => {
  let component: FormContribuintesComponent;
  let fixture: ComponentFixture<FormContribuintesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormContribuintesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormContribuintesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
