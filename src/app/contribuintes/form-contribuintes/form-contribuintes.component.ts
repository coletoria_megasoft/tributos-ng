import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { Contribuinte } from 'src/app/contribuintes/contribuinte';
import { ContribuintesService } from 'src/app/contribuintes/contribuintes.service';

@Component({
  selector: 'app-form-contribuintes',
  templateUrl: './form-contribuintes.component.html',
  styleUrls: ['./form-contribuintes.component.css']
})
export class FormContribuintesComponent implements OnInit {

  contribuinte = new Contribuinte();

  constructor(private route: ActivatedRoute,
    private router: Router,
    private service: ContribuintesService
  ) { }

  ngOnInit() {
    this.route.params
    .pipe(
      map((params: any) => params.id),
      switchMap((id) => {
        return id ? this.service.pesquisarPorId(id) : new Observable<Contribuinte>();
      }),
    ).subscribe((contribuinte) => this.contribuinte = contribuinte);
  }

  salvar() {
    this.service.salvar(this.contribuinte).subscribe((dados) => {
      console.log(dados);
      this.router.navigate(['/contribuintes']);
    });
  }
}
