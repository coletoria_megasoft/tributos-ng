import { TestBed } from '@angular/core/testing';

import { ContribuintesService } from './contribuintes.service';

describe('ContribuintesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContribuintesService = TestBed.get(ContribuintesService);
    expect(service).toBeTruthy();
  });
});
