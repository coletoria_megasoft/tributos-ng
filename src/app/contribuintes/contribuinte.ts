export class Contribuinte {

  id: number;
  nome: string;
  cpfcnpj: string;
  endereco: string;
  cidade: string;
  estado: string;
  telefone: string;
  celular: string;

  constructor() {
    this.id = null;
    this.nome = null;
    this.cpfcnpj = null;
    this.endereco = null;
    this.cidade = null;
    this.estado = null;
    this.telefone = null;
    this.celular = null;
  }

}