import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Contribuinte } from './contribuinte';

@Injectable({
  providedIn: 'root'
})
export class ContribuintesService {

  baseURL = `${environment.API_URL}`;
  url = `${this.baseURL}/contribuintes`;
    
  constructor(private http: HttpClient) { }

  listar(): Observable<Contribuinte[]> {
    return this.http.get<Contribuinte[]>(this.url).pipe(take(1));
  }

  pesquisarPorId(id: number): Observable<Contribuinte> {
    return this.http.get<Contribuinte>(`${this.url}/${id}`).pipe(take(1));
  }

  salvar(contribuinte: Contribuinte) {
    if (contribuinte.id) {
      return this.http.put<Contribuinte>(this.url, contribuinte).pipe(take(1));
    } else {
      return this.http.post<Contribuinte>(this.url, contribuinte).pipe(take(1));
    }
  }

  deletarPorId(id: number): any {
    return this.http.delete<Contribuinte>(`${this.url}/${id}`).pipe(take(1));
  }
}
