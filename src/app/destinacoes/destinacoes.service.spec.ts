import { TestBed } from '@angular/core/testing';

import { DestinacoesService } from './destinacoes.service';

describe('DestinacoesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DestinacoesService = TestBed.get(DestinacoesService);
    expect(service).toBeTruthy();
  });
});
