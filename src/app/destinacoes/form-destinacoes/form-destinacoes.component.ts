import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { Destinacao } from '../destinacao';
import { DestinacoesService } from '../destinacoes.service';

@Component({
  selector: 'app-form-destinacoes',
  templateUrl: './form-destinacoes.component.html',
  styleUrls: ['./form-destinacoes.component.css']
})
export class FormDestinacoesComponent implements OnInit {

  destinacao = new Destinacao();

  constructor(private route: ActivatedRoute,
    private router: Router,
    private service: DestinacoesService
  ) { }

  ngOnInit() {
    this.route.params
    .pipe(
      map((params: any) => params.id),
      switchMap((id) => {
        return id ? this.service.pesquisarPorId(id) : new Observable<Destinacao>();
      }),
    ).subscribe((destinacao) => this.destinacao = destinacao);
  }

  salvar() {
    this.service.salvar(this.destinacao).subscribe((dados) => {
      console.log(dados);
      this.router.navigate(['/destinacoes']);
    });
  }

}
