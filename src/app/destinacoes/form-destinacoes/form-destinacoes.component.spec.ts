import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormDestinacoesComponent } from './form-destinacoes.component';

describe('FormDestinacoesComponent', () => {
  let component: FormDestinacoesComponent;
  let fixture: ComponentFixture<FormDestinacoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormDestinacoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormDestinacoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
