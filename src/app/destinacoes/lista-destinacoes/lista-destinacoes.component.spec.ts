import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaDestinacoesComponent } from './lista-destinacoes.component';

describe('ListaDestinacoesComponent', () => {
  let component: ListaDestinacoesComponent;
  let fixture: ComponentFixture<ListaDestinacoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaDestinacoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaDestinacoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
