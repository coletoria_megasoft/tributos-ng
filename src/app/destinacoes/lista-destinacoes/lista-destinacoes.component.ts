import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Destinacao } from '../destinacao';
import { DestinacoesService } from '../destinacoes.service';

@Component({
  selector: 'app-lista-destinacoes',
  templateUrl: './lista-destinacoes.component.html',
  styleUrls: ['./lista-destinacoes.component.css']
})
export class ListaDestinacoesComponent implements OnInit {

  destinacoes$: Observable<Destinacao[]>;

  constructor(private service: DestinacoesService,) { }

  ngOnInit() {
    this.destinacoes$ = this.service.listar();
  }

}
