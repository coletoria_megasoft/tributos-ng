import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Destinacao } from './destinacao';

@Injectable({
  providedIn: 'root'
})
export class DestinacoesService {

  baseURL = `${environment.API_URL}`;
  url = `${this.baseURL}/destinacoes`;
    
  constructor(private http: HttpClient) { }

  listar(): Observable<Destinacao[]> {
    return this.http.get<Destinacao[]>(this.url).pipe(take(1));
  }

  pesquisarPorId(id: number): Observable<Destinacao> {
    return this.http.get<Destinacao>(`${this.url}/${id}`).pipe(take(1));
  }

  salvar(destinacao: Destinacao) {
    if (destinacao.id) {
      return this.http.put<Destinacao>(this.url, destinacao).pipe(take(1));
    } else {
      return this.http.post<Destinacao>(this.url, destinacao).pipe(take(1));
    }
  }

  deletarPorId(id: number): any {
    return this.http.delete<Destinacao>(`${this.url}/${id}`).pipe(take(1));
  }

}
