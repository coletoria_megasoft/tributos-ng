import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cpfcnpj'
})
export class CpfcnpjPipe implements PipeTransform {

  transform(value: any): string {
    
    var cpfCnpjValor = value.replace(/\D/g, '');
    
    if (cpfCnpjValor.length === 11) {
      value = this.transformarCPF(cpfCnpjValor);
    } else if (cpfCnpjValor.length === 14) {
      value = this.transformarCNPJ(cpfCnpjValor);
    }
    
    return value;

  }

  transformarCPF(cpf: string): string {

    var cpfValor = cpf.replace(/\D/g, '');

    var cpfLista = cpfValor.match(/^(\d{3})(\d{3})(\d{3})(\d{2})$/);
    
    if (cpfLista && cpfLista.length === 5) {
      cpf = cpfLista[1] + '.' + cpfLista[2] + '.' + cpfLista[3] + '-' + cpfLista[4];
    }
    
    return cpf;

  }

  transformarCNPJ(cnpj: string): string {
  
    var cnpjValor = cnpj.replace(/\D/g, '');
    
    if (cnpjValor.length !== 14) {
      return cnpj;
    }
    
    var cnpjLista = cnpjValor.match(/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})$/);
    
    if (cnpjLista && cnpjLista.length === 6) {
      cnpj = cnpjLista[1] + '.' + cnpjLista[2] + '.' + cnpjLista[3] + '/' + cnpjLista[4] + '-' + cnpjLista[5];
    }
    
    return cnpj;

  }
}
