import { TestBed } from '@angular/core/testing';

import { RefisService } from './refis.service';

describe('RefisService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RefisService = TestBed.get(RefisService);
    expect(service).toBeTruthy();
  });
});
