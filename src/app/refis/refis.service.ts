import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Refis } from '../tributos/refis';

@Injectable({
  providedIn: 'root'
})
export class RefisService {

  baseURL = `${environment.API_URL}`;
    
  constructor(private http: HttpClient) { }

  listar(): Observable<Refis[]> {
    const url = `${this.baseURL}/refis`;
    return this.http.get<Refis[]>(url).pipe(take(1));
  }
  
}
