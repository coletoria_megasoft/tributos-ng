import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaRefisComponent } from './lista-refis.component';

describe('ListaRefisComponent', () => {
  let component: ListaRefisComponent;
  let fixture: ComponentFixture<ListaRefisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaRefisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaRefisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
