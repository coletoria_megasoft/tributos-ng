import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Refis } from 'src/app/tributos/refis';
import { RefisService } from '../refis.service';

@Component({
  selector: 'app-lista-refis',
  templateUrl: './lista-refis.component.html',
  styleUrls: ['./lista-refis.component.css']
})
export class ListaRefisComponent implements OnInit {

  refis$: Observable<Refis[]>;

  constructor(private service: RefisService) { }

  ngOnInit() {
    this.refis$ = this.service.listar();
  }

}
