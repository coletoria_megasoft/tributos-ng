import { Tributo } from "../tributos/tributo";

export class Refis {

    id: number;
    dataInicio: Date;
    dataFim: Date;
    ativo: boolean;
    tipo: string;
    taxaDescontoJuros: number;
    taxaDescontoMulta: number;
    tributo: Tributo;

    constructor() {
        this.id = null;
        this.dataInicio = null;
        this.dataFim = null;
        this.ativo = false;
        this.tipo = null;
        this.taxaDescontoJuros = null;
        this.taxaDescontoMulta = null;
        this.tributo = new Tributo();
    }

}
