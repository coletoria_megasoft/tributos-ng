import { Contribuinte } from "../contribuintes/contribuinte";
import { Tributo } from "../tributos/tributo";

export class Duam {

    id: number;
    valor: number;
    tipo: string;
    dataVencimento: Date;
    situacao: string;
    dataPagamento: Date;
    contribuinte: Contribuinte;
    tributo: Tributo;

    constructor() {
        this.id = null;
        this.valor = null;
        this.tipo = null;
        this.dataVencimento = null;
        this.situacao = null;
        this.dataPagamento = null;
        this.contribuinte = new Contribuinte();
        this.tributo = new Tributo();
    }

}
