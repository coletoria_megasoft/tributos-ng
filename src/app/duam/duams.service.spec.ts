import { TestBed } from '@angular/core/testing';

import { DuamsService } from './duams.service';

describe('DuamsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DuamsService = TestBed.get(DuamsService);
    expect(service).toBeTruthy();
  });
});
