import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Duam } from './duam';

@Injectable({
  providedIn: 'root'
})
export class DuamsService {

  baseURL = `${environment.API_URL}`;
    
  constructor(private http: HttpClient) { }

  listar(): Observable<Duam[]> {
    const url = `${this.baseURL}/duams`;
    return this.http.get<Duam[]>(url).pipe(take(1));
  }

  pesquisarPorContribuinte(contribuinteId: number) {
    const url = `${this.baseURL}/duams/?contribuinte=${contribuinteId}`;
    return this.http.get<Duam[]>(url).pipe(take(1));
  }

}
