import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaDuamsComponent } from './lista-duams.component';

describe('ListaDuamsComponent', () => {
  let component: ListaDuamsComponent;
  let fixture: ComponentFixture<ListaDuamsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaDuamsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaDuamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
