import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Contribuinte } from 'src/app/contribuintes/contribuinte';
import { ContribuintesService } from 'src/app/contribuintes/contribuintes.service';
import { Duam } from '../duam';
import { DuamsService } from '../duams.service';

@Component({
  selector: 'app-lista-duams',
  templateUrl: './lista-duams.component.html',
  styleUrls: ['./lista-duams.component.css']
})
export class ListaDuamsComponent implements OnInit {

  duams$: Observable<Duam[]>;
  contribuintes$: Observable<Contribuinte[]>;
  contribuinte: number = 0;

  constructor(private service: DuamsService,
    private contribuinteService: ContribuintesService) { }

  ngOnInit() {
    this.duams$ = this.service.listar();
    this.contribuintes$ = this.contribuinteService.listar();
  }

  pesquisarContribuinte() {
    this.duams$ = this.service.pesquisarPorContribuinte(this.contribuinte);
  }

}
